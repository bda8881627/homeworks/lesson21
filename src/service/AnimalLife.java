package service;

public class AnimalLife extends Thread{
    public int energy =100;
    public String mood;

    public void run() {
        while (energy > 0) {
            if (energy >= 9) {
                energy =energy - 9;
            } else {
                energy = 0;
            }

            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public void stopThread() {
    }
}
