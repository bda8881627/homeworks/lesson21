package service;

import model.Enums;

import java.util.Scanner;

public class AnimalService implements Animalnter{

    static AnimalLife animalThread=new AnimalLife();

    public static void start() {
        animalThread.start();
    }



    Scanner s=new Scanner(System.in);
    @Override
    public void showMenu() {
        boolean exit = false;
        animalThread.start();
        while (!exit) {
            System.out.println("Сhoose one of the options:");
            System.out.println("1. Eat meal (+9%)");
            System.out.println("2. Drink milk (+8%)");
            System.out.println("3. Show energy ");

            int select=s.nextInt();

            switch (select){
                case 1:
                    eat();
                    break;
                case 2:
                    drink();
                    break;
                case 3:
                    showEnergy();
                    break;
                default:
                    System.out.println("Error. Please try again");
            }


        }
          animalThread.stopThread();
        System.out.println("exit");
    }

    @Override
    public void eat() {
        animalThread.energy+=9;

        if (animalThread.energy>=Enums.GOOD.i){
            animalThread.mood=Enums.GOOD.name();
            System.out.println("Her sey eladi "+ animalThread.mood);
        }
        else if(animalThread.energy<Enums.GOOD.i && animalThread.energy>Enums.NORMAL.i ){
            animalThread.mood=Enums.NORMAL.name();
            System.out.println("Her sey yaxsidi "+ animalThread.mood);
        }
        else if(animalThread.energy<Enums.NORMAL.i && animalThread.energy>Enums.BAD.i ){
            animalThread.mood=Enums.BAD.name();
            System.out.println("Her sey pisdi "+ animalThread.mood);
        }
        else if(animalThread.energy<Enums.BAD.i && animalThread.energy>Enums.DEATH.i ){
            animalThread.mood=Enums.DEATH.name();
            System.out.println("Her sey oldu "+ animalThread.mood);
        }
        System.out.println("Mood " + animalThread.mood);
        System.out.println("enerjini 9% artırır " + animalThread.energy);


    }

    @Override
    public void drink() {
        animalThread.energy+=8;

        if (animalThread.energy>=Enums.GOOD.i){
            animalThread.mood=Enums.GOOD.name();
            System.out.println("Her sey eladi "+ animalThread.mood);
        }
        else if(animalThread.energy<Enums.GOOD.i && animalThread.energy>Enums.NORMAL.i ){
            animalThread.mood=Enums.NORMAL.name();
            System.out.println("Her sey yaxsidi "+ animalThread.mood);
        }
        else if(animalThread.energy<Enums.NORMAL.i && animalThread.energy>Enums.BAD.i ){
            animalThread.mood=Enums.BAD.name();
            System.out.println("Her sey pisdi "+ animalThread.mood);
        }
        else if(animalThread.energy<Enums.BAD.i && animalThread.energy>Enums.DEATH.i ){
            animalThread.mood=Enums.DEATH.name();
            System.out.println("Her sey oldu "+ animalThread.mood);
        }

        System.out.println("Mood " + animalThread.mood);
        System.out.println("enerjini 8% artırır " + animalThread.energy);


    }

    @Override
    public void showEnergy() {

        if (animalThread.energy>=Enums.GOOD.i){
            animalThread.mood=Enums.GOOD.name();
            System.out.println("Her sey eladi "+ animalThread.mood);
        }
        else if(animalThread.energy<Enums.GOOD.i && animalThread.energy>Enums.NORMAL.i ){
            animalThread.mood=Enums.NORMAL.name();
            System.out.println("Her sey yaxsidi "+ animalThread.mood);
        }
        else if(animalThread.energy<Enums.NORMAL.i && animalThread.energy>Enums.BAD.i ){
            animalThread.mood=Enums.BAD.name();
            System.out.println("Her sey pisdi "+ animalThread.mood);
        }
        else if(animalThread.energy<Enums.BAD.i && animalThread.energy>Enums.DEATH.i ){
            animalThread.mood=Enums.DEATH.name();
            System.out.println("Her sey oldu "+ animalThread.mood);
        }
        System.out.println("Mood " + animalThread.mood);
        System.out.println("hal-hazırkı enerjimizi " + animalThread.energy);


    }
}
